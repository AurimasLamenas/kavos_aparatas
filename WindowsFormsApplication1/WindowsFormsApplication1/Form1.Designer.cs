﻿namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ekranas = new System.Windows.Forms.TextBox();
            this.mygtukasJuoda = new System.Windows.Forms.Button();
            this.mygtukasBalta = new System.Windows.Forms.Button();
            this.mygtukasLate = new System.Windows.Forms.Button();
            this.mygtukasMocha = new System.Windows.Forms.Button();
            this.mygtukasCapu = new System.Windows.Forms.Button();
            this.mygtukasReset = new System.Windows.Forms.Button();
            this.mygtukasLog = new System.Windows.Forms.Button();
            this.mygtukasPopuliariausia = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ekranas
            // 
            this.ekranas.Location = new System.Drawing.Point(12, 35);
            this.ekranas.Multiline = true;
            this.ekranas.Name = "ekranas";
            this.ekranas.Size = new System.Drawing.Size(152, 265);
            this.ekranas.TabIndex = 0;
            this.ekranas.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // mygtukasJuoda
            // 
            this.mygtukasJuoda.Location = new System.Drawing.Point(199, 97);
            this.mygtukasJuoda.Name = "mygtukasJuoda";
            this.mygtukasJuoda.Size = new System.Drawing.Size(75, 23);
            this.mygtukasJuoda.TabIndex = 1;
            this.mygtukasJuoda.Text = "Juoda";
            this.mygtukasJuoda.UseVisualStyleBackColor = true;
            this.mygtukasJuoda.Click += new System.EventHandler(this.mygtukasJuoda_Click);
            // 
            // mygtukasBalta
            // 
            this.mygtukasBalta.Location = new System.Drawing.Point(199, 141);
            this.mygtukasBalta.Name = "mygtukasBalta";
            this.mygtukasBalta.Size = new System.Drawing.Size(75, 23);
            this.mygtukasBalta.TabIndex = 2;
            this.mygtukasBalta.Text = "Balta";
            this.mygtukasBalta.UseVisualStyleBackColor = true;
            this.mygtukasBalta.Click += new System.EventHandler(this.mygtukasBalta_Click);
            // 
            // mygtukasLate
            // 
            this.mygtukasLate.Location = new System.Drawing.Point(199, 185);
            this.mygtukasLate.Name = "mygtukasLate";
            this.mygtukasLate.Size = new System.Drawing.Size(75, 23);
            this.mygtukasLate.TabIndex = 3;
            this.mygtukasLate.Text = "Late";
            this.mygtukasLate.UseVisualStyleBackColor = true;
            this.mygtukasLate.Click += new System.EventHandler(this.mygtukasLate_Click);
            // 
            // mygtukasMocha
            // 
            this.mygtukasMocha.Location = new System.Drawing.Point(199, 230);
            this.mygtukasMocha.Name = "mygtukasMocha";
            this.mygtukasMocha.Size = new System.Drawing.Size(75, 23);
            this.mygtukasMocha.TabIndex = 4;
            this.mygtukasMocha.Text = "Mocha";
            this.mygtukasMocha.UseVisualStyleBackColor = true;
            this.mygtukasMocha.Click += new System.EventHandler(this.mygtukasMocha_Click);
            // 
            // mygtukasCapu
            // 
            this.mygtukasCapu.Location = new System.Drawing.Point(199, 277);
            this.mygtukasCapu.Name = "mygtukasCapu";
            this.mygtukasCapu.Size = new System.Drawing.Size(75, 23);
            this.mygtukasCapu.TabIndex = 5;
            this.mygtukasCapu.Text = "Capuccino";
            this.mygtukasCapu.UseVisualStyleBackColor = true;
            this.mygtukasCapu.Click += new System.EventHandler(this.mygtukasCapu_Click);
            // 
            // mygtukasReset
            // 
            this.mygtukasReset.Location = new System.Drawing.Point(377, 230);
            this.mygtukasReset.Name = "mygtukasReset";
            this.mygtukasReset.Size = new System.Drawing.Size(75, 23);
            this.mygtukasReset.TabIndex = 6;
            this.mygtukasReset.Text = "Reset";
            this.mygtukasReset.UseVisualStyleBackColor = true;
            this.mygtukasReset.Click += new System.EventHandler(this.mygtukasReset_Click);
            // 
            // mygtukasLog
            // 
            this.mygtukasLog.Location = new System.Drawing.Point(377, 277);
            this.mygtukasLog.Name = "mygtukasLog";
            this.mygtukasLog.Size = new System.Drawing.Size(75, 23);
            this.mygtukasLog.TabIndex = 7;
            this.mygtukasLog.Text = "Log";
            this.mygtukasLog.UseVisualStyleBackColor = true;
            this.mygtukasLog.Click += new System.EventHandler(this.mygtukasLog_Click);
            // 
            // mygtukasPopuliariausia
            // 
            this.mygtukasPopuliariausia.Location = new System.Drawing.Point(377, 185);
            this.mygtukasPopuliariausia.Name = "mygtukasPopuliariausia";
            this.mygtukasPopuliariausia.Size = new System.Drawing.Size(75, 23);
            this.mygtukasPopuliariausia.TabIndex = 8;
            this.mygtukasPopuliariausia.Text = "populiariausia";
            this.mygtukasPopuliariausia.UseVisualStyleBackColor = true;
            this.mygtukasPopuliariausia.Click += new System.EventHandler(this.mygtukasPopuliariausia_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(494, 339);
            this.Controls.Add(this.mygtukasPopuliariausia);
            this.Controls.Add(this.mygtukasLog);
            this.Controls.Add(this.mygtukasReset);
            this.Controls.Add(this.mygtukasCapu);
            this.Controls.Add(this.mygtukasMocha);
            this.Controls.Add(this.mygtukasLate);
            this.Controls.Add(this.mygtukasBalta);
            this.Controls.Add(this.mygtukasJuoda);
            this.Controls.Add(this.ekranas);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox ekranas;
        private System.Windows.Forms.Button mygtukasJuoda;
        private System.Windows.Forms.Button mygtukasBalta;
        private System.Windows.Forms.Button mygtukasLate;
        private System.Windows.Forms.Button mygtukasMocha;
        private System.Windows.Forms.Button mygtukasCapu;
        private System.Windows.Forms.Button mygtukasReset;
        private System.Windows.Forms.Button mygtukasLog;
        private System.Windows.Forms.Button mygtukasPopuliariausia;
    }
}

