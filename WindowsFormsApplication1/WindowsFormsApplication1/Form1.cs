﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        const int MaxPuodukuSkaicius = 10;

        int puodukuSkaicius = 0;
        string[] logas = new string[10];

        public Form1()
        {
            InitializeComponent();
        }

        private void mygtukasJuoda_Click(object sender, EventArgs e)
        {
            Atvaizduok("Juoda");
        }

        private void mygtukasBalta_Click(object sender, EventArgs e)
        {
            Atvaizduok("Balta");
        }

        private void mygtukasLate_Click(object sender, EventArgs e)
        {
            Atvaizduok("Late");
        }

        private void mygtukasMocha_Click(object sender, EventArgs e)
        {
            Atvaizduok("Mocha");
        }

        private void mygtukasCapu_Click(object sender, EventArgs e)
        {
            Atvaizduok("Capuccino");
        }

        private void Atvaizduok(string kava)
        {
            if (puodukuSkaicius >= MaxPuodukuSkaicius)
            {
                ekranas.Text = ("baigesi kava");
                return;
            }


            for (int i = 0; i < 100; i++)
            {
                ekranas.Text = i.ToString();
                Thread.Sleep(10);
                ekranas.Refresh();
            }
            ekranas.Text = kava + " kava paruosta";

            logas[puodukuSkaicius] = kava;
            puodukuSkaicius++;
        }

        private void mygtukasReset_Click(object sender, EventArgs e)
        {
            Form2 testDialog = new Form2();

            DialogResult result = testDialog.ShowDialog(this);

            if (result == DialogResult.OK)
            {
                if (testDialog.slaptazodis.Text == "123")
                {
                    ekranas.Text = "";
                    puodukuSkaicius = 0;

                    for (int i = 0; i < logas.Length; i++)
                    {
                        logas[i] = "";
                    }

                    MessageBox.Show("Reset ok");


                }
            }
        }

        private void mygtukasLog_Click(object sender, EventArgs e)
        {
            ekranas.Text = "Ataskaita:" + Environment.NewLine;
            foreach (string kava in logas)
            {
                ekranas.Text += kava + Environment.NewLine;
            }


        }

        private void mygtukasPopuliariausia_Click(object sender, EventArgs e)
        {
            int[] kavos = new int[5];

            foreach (string kava in logas)
            {
                if (kava == "Juoda") ;
                {
                    kavos[0] += 1;
                }
                if (kava == "Balta") ;
                {
                    kavos[1] += 1;
                }
                if (kava == "Late") ;
                {
                    kavos[2] += 1;
                }
                if (kava == "Mocha") ;
                {
                    kavos[3] += 1;
                }
                if (kava == "Capuccino") ;
                {
                    kavos[4] += 1;
                }




            }
        }
    }
}


     


              
            
    
 

    

